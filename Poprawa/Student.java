package pl.edu.uwm.wmii.mrozowskimarcel.m;

import java.time.LocalDate;

public class Student implements Cloneable, Comparable<Student> {
    public Student(String nazwa, LocalDate dataUrodziny, double ocena)
    {
        this.nazwa=nazwa;
        this.dataUrodziny=dataUrodziny;
        this.ocena=ocena;
    }

    private String nazwa;
    private LocalDate dataUrodziny;
    private double ocena;

    public String getNazwa()
    {
        return nazwa;
    }

    public LocalDate getDataUrodziny()
    {
        return dataUrodziny;
    }

    public double getOcena()
    {
        return ocena;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+" [Nazwa: "+getNazwa()+" Data urodziny: "+getDataUrodziny()+" Ocena: " + getOcena()+"]";
    }

    @Override
    public int compareTo(Student other)
    {
        int porownanie=this.dataUrodziny.compareTo(other.dataUrodziny);
        if(porownanie==0)
        {
            int porownanie2=this.nazwa.compareTo(other.nazwa);
            if(porownanie2==0)
            {
                return -(int)Math.ceil(this.ocena-other.ocena);
            }
            return porownanie2;
        }
        return porownanie;
    }
}