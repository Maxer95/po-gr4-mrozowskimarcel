package pl.edu.uwm.wmii.mrozowskimarcel.m;

import java.time.LocalDate;
import java.util.ArrayList;

public class Stypendium {
    public Stypendium(double kwotaStypendium, ArrayList<Student> lista){
        Stypendium.kwotaStypendium = kwotaStypendium;
        this.lista = lista;
    }

    static double kwotaStypendium=0;
    private ArrayList<Student> lista;

    public static void setKwotaStypendium(double kwotaStypendium)
    {
        Stypendium.kwotaStypendium = kwotaStypendium;
    }

    public void setLista(Student student)
    {
        lista.add(student);
    }

        public static void setKwotaStypendium(double kwotaStypendium) {
        for(Stundent s : lista) {
            if(s.dataUrodzenia.year > 2005 & s.ocena == 5) {
                this.kwotaStypendium = 500;
            }
        }
    }

    public static void main(String[] args) {
        Student Rafal = new Student("Rafal Wysocki", LocalDate.of(2002, 5, 12), 5);
        Student Marcin = new Student("Marcin Ptaszek", LocalDate.of(2002, 5, 12), 4);
        Student Konrad = new Student("Konrad Muszynski", LocalDate.of(1986, 2, 7), 4);
        Student Kuba = new Student("Kuba Muszynski", LocalDate.of(2000, 12, 25), 3.5);
        setLista(Rafal);
        setLista(Marcin);
        setLista(Konrad);
        setLista(Kuba);
    }
}