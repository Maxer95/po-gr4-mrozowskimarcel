package com.company.Kolokwium2.pl.imiajd.mrozowski;

    public Autor(String nazwa, String email, char plec) {
        this.nazwa = nazwa;
        this.email = email;
        this.plec = plec;}
 
    public String getNazwa(){
        return this.nazwa;}
		
    public String getEmail() { return this.email; }
    
	public char getPlec() { return this.plec; }
 
    public void setNazwa (String nazwa){
        this.nazwa = nazwa;}
    
	public void setEmail (String email){
        this.email = email;}
    
	public void setPlec (char plec){
        this.plec = plec;}
    
	@Override
    public int compareTo(Autor o) {
        int compare_nazwa = this.nazwa.compareTo(o.nazwa);
        if(compare_nazwa==0){
            return this.plec-o.plec;}
        return compare_nazwa;}
 
    @Override
    public boolean equals(Object obj){
        return this.toString().equals(obj.toString());}
 
    public Autor clone() {
        try {
            return (Autor) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();}}
 
    @Override
    public String toString(){
        return this.getClass().getSimpleName()+" Autor : [Nazwisko:"+this.nazwa+" Email: "+this.email+"Plec"+this.plec+"]\n";}
    private String nazwa;
    private String email;
    private char plec;}