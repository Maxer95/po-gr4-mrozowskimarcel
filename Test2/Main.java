package com.company.Kolokwium2;
import com.company.Kolokwium2.pl.imiajd.mrozowski.*;
import java.time.LocalDate;
import java.util.*;

public static void main(String args[]) {
        ArrayList<Autor> grupa = new ArrayList<>();
        grupa.add(0, new Autor("Marek","marek123@kappa.kappa",'m'));
        grupa.add(1, new Autor("Marek","marek312@kappa.kappa",'m'));
        grupa.add(2, new Autor("Kasia","Kasia@kappa.kappa",'k'));
        grupa.add(3, new Autor("Basia","Basia@kappa.kappa",'k'));
        System.out.println("BefSort:");
        for(int i = 0 ; i < grupa.size() ; i++){
            System.out.print(grupa.get(i).toString());}
        Collections.sort(grupa);
        System.out.println("PostSort:");
        for(int i = 0 ; i < grupa.size() ; i++){
            System.out.print(grupa.get(i).toString());}
 
        System.out.println("\n\n");
 
        ArrayList<Ksiazka> Listaksiazek = new ArrayList<>();
        Listaksiazek.add(0, new Ksiazka("Jan Fesola",grupa.get(0),100.0));
        Listaksiazek.add(1, new Ksiazka("Bonifacy",grupa.get(0),50.0));
        Listaksiazek.add(2, new Ksiazka("Głodówka w lesie węży",grupa.get(1),100.0));
        Listaksiazek.add(3, new Ksiazka("Marcin Dubiel na Zakręcie",grupa.get(2),100.0));
        System.out.println("Przed Sortowaniem :");
        for(int i = 0 ; i < Listaksiazek.size() ; i++){
            System.out.print(Listaksiazek.get(i).toString());}
        Collections.sort(Listaksiazek);
        System.out.println("Po Sortowaniu :");
        for(int i = 0 ; i < Listaksiazek.size() ; i++){
            System.out.print(Listaksiazek.get(i).toString());}
 
        System.out.println("\nZadanie2\n");
 
        LinkedList<Ksiazka> ksiazki = new LinkedList<Ksiazka>();
 
        ksiazki.add(0, new Ksiazka("Sklepy Cynamonowe",grupa.get(0),100.0));
        ksiazki.add(1, new Ksiazka("CDaction",grupa.get(0),50.0));
        ksiazki.add(2, new Ksiazka("Krzyzacy",grupa.get(1),100.0));
        ksiazki.add(3, new Ksiazka("Maly Ksiaze",grupa.get(2),100.0));
        System.out.println("BefDel");
        System.out.print(ksiazki);
        System.out.println("PostDel");
        RedukujKol2.redukuj(ksiazki, 2);}